package mobilne.avachi.com.projekt_mobilne.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import java.sql.Date;
import java.time.OffsetDateTime;

@Entity(tableName = "stat_table")
public class Stat {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @NonNull
    @ColumnInfo(name="day")
    private String data;


    private long steps;


    private long calories;

    public Stat(@NonNull String data, @NonNull long steps, @NonNull long calories) {
        this.data = data;
        this.steps = steps;
        this.calories = calories;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @NonNull
    public String getData() {
        return data;
    }

    public void setData(@NonNull String data) {
        this.data = data;
    }

    public long getSteps() {
        return steps;
    }

    public void setSteps(long steps) {
        this.steps = steps;
    }

    public long getCalories() {
        return calories;
    }

    public void setCalories(long calories) {
        this.calories = calories;
    }
}
