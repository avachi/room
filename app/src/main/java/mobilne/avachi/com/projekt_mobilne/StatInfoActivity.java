package mobilne.avachi.com.projekt_mobilne;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import mobilne.avachi.com.projekt_mobilne.database.Stat;
import mobilne.avachi.com.projekt_mobilne.database.StatViewModel;

public class StatInfoActivity extends AppCompatActivity {

    public static StatViewModel statViewModel;

    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        statViewModel = ViewModelProviders.of(this).get(StatViewModel.class);
        statViewModel.getAllStats().observe(this, new Observer<List<Stat>>() {
            @Override
            public void onChanged(@Nullable List<Stat> stats) {
                recyclerViewAdapter.updateList(new ArrayList<Stat>(stats));
                recyclerViewAdapter.notifyDataSetChanged();
            }
        });

        recyclerView = findViewById(R.id.recycleView);
        recyclerViewAdapter = new RecyclerViewAdapter(this, new ArrayList<Stat>());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Intent intent;
        switch(item.getItemId()){
            case R.id.menu:
                intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuSettings:
                intent = new Intent(this, SettingsActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuStats:
                intent = new Intent(this, StatInfoActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuNews:
                intent = new Intent(this, NewsActivity.class);
                this.startActivity(intent);
                break;
        }
        return true;
    }
}
