package mobilne.avachi.com.projekt_mobilne;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;

import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Date;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Calendar;

import mobilne.avachi.com.projekt_mobilne.database.Stat;
import mobilne.avachi.com.projekt_mobilne.database.StatViewModel;


public class MainActivity extends AppCompatActivity implements SensorEventListener{
    private final String CHANNEL_ID = "personal_notification";
    private final int NOTIFICATION_ID = 001;
    SensorManager sensorManager;
    TextView countTextView;
    TextView distTextView;
    TextView calTextView;
    TextView goalTextView;
    TextView heartValue;
    boolean flag = false;
    Stat statt;

    float weight;
    double distance;
    int goal;
    boolean sex;

    Long todaySteps;
    long todayCalories;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        SharedPreferences sharedPreferences = getSharedPreferences("dane", MODE_PRIVATE);
        weight = sharedPreferences.getFloat("weight", 30);
        goal  = sharedPreferences.getInt("goal", 0);
        sex = sharedPreferences.getBoolean("male", true);

        countTextView = findViewById(R.id.todayCountValue);
        distTextView = findViewById(R.id.speedValue);
        calTextView = findViewById(R.id.calTextValue);
        goalTextView = findViewById(R.id.goalTextValue);
        heartValue = findViewById(R.id.heartValue);
        if(goal != 0) goalTextView.setText("/"+goal);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if(flag == false){
            todaySteps = 0L;
        }

      StatViewModel statViewModel = ViewModelProviders.of(this).get(StatViewModel.class);


        statViewModel.getStat(new Date(System.currentTimeMillis()).toString()).observe(this, new Observer<Stat>() {
            @Override
            public void onChanged(@Nullable Stat stat) {

                if(stat != null) {
                    statt = stat;
                    if(flag == false) {
                        todaySteps = stat.getSteps() + todaySteps;
                        flag = true;
                    }
                }
            }
        });

        if(!sharedPreferences.getBoolean("hi5", false)){
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //sensor counter part

        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        Sensor heartSensor = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        if(countSensor!=null){
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }else{
            Toast.makeText(this, R.string.sensor_not_found , Toast.LENGTH_SHORT).show();
        }

        if(heartSensor != null){
            sensorManager.registerListener(this, heartSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }else{
            Toast.makeText(this, R.string.heart_sensor_not_found , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        StatViewModel statViewModel = ViewModelProviders.of(this).get(StatViewModel.class);
        if(statt != null) {
            statt.setCalories(todayCalories);
            statt.setSteps(todaySteps);
            statViewModel.update(statt);
        } else if(statt == null && todaySteps > 10){
            statt = new Stat(new Date(System.currentTimeMillis()).toString(), todaySteps, todayCalories);
            statViewModel.insert(statt);
        }
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

    if(event.sensor.getType() == Sensor.TYPE_STEP_COUNTER){
        DecimalFormat df = new DecimalFormat("#.###");
        todaySteps++;
        countTextView.setText(String.valueOf(todaySteps));
        distance = calculateDistance(todaySteps);
        distTextView.setText(String.valueOf(distance));
        todayCalories = (long) (distance * weight * 1.0363);
        calTextView.setText(String.valueOf(todayCalories));

        if(todaySteps == goal){
            displayNotification();
        }
    }

    if(event.sensor.getType() == Sensor.TYPE_HEART_RATE){
        heartValue.setText(Float.toString(event.values.length>0 ? event.values[0] : 0));
    }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
    public void displayNotification(){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_event_finished);
        builder.setContentTitle(getResources().getString(R.string.goalTitle));
        builder.setContentText(getResources().getString(R.string.goalNotification));
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }
    public double calculateDistance(float steps){
        double val = 0;
        if(sex == true) val = steps * 78/100000;
        else val = steps * 72 / 100000;
        val *= 100;
        val = Math.round(val);
        val /= 100;
        return val;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Intent intent;
        switch(item.getItemId()){
            case R.id.menu:
                intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuSettings:
                intent = new Intent(this, SettingsActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuStats:
                intent = new Intent(this, StatInfoActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuNews:
                intent = new Intent(this, NewsActivity.class);
                this.startActivity(intent);
                break;
        }
        return true;
    }

}

