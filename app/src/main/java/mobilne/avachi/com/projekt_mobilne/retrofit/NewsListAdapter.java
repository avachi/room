package mobilne.avachi.com.projekt_mobilne.retrofit;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mobilne.avachi.com.projekt_mobilne.R;
import mobilne.avachi.com.projekt_mobilne.RecyclerViewAdapter;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<News> newsArrayList;


    public NewsListAdapter(Context context, ArrayList<News> newsArrayList) {
        this.context = context;
        this.newsArrayList = newsArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.topic.setText(newsArrayList.get(i).getTopic());
        viewHolder.desc.setText(newsArrayList.get(i).getDescription());
        Glide.with(context).load(newsArrayList.get(i).getImage()).into(viewHolder.image);
    }

    @Override
    public int getItemCount() {
        return newsArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView topic;
        private TextView desc;
        private ImageView image;
        public ViewHolder(View v){
            super(v);
            topic = v.findViewById(R.id.textTopic);
            desc = v.findViewById(R.id.textDescription);
            image = v.findViewById(R.id.img);
        }
    }


}
