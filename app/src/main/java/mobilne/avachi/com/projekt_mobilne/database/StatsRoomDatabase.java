package mobilne.avachi.com.projekt_mobilne.database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Stat.class}, version = 1, exportSchema = false)
public abstract class StatsRoomDatabase extends RoomDatabase {
    public abstract StatDao statDao();

    private static  volatile StatsRoomDatabase INSTANCE;

    static StatsRoomDatabase getDatabase(final Context contex){
        if(INSTANCE == null){
            synchronized (StatsRoomDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(contex.getApplicationContext(), StatsRoomDatabase.class, "stats_database").build();
                }
            }
        }
        return INSTANCE;
    }
}
