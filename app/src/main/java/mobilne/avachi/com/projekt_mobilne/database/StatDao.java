package mobilne.avachi.com.projekt_mobilne.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.sql.Date;
import java.time.OffsetDateTime;
import java.util.List;

@Dao
public interface StatDao {

    @Insert
    void insert(Stat stat);

    @Update
    void update(Stat stat);

    @Delete
    void delete(Stat stat);

    @Query("SELECT * FROM stat_table WHERE day LIKE  :date LIMIT 1")
    LiveData<Stat> findByDay(String date);

    @Query("SELECT * FROM stat_table ORDER BY id DESC LIMIT 30")
    LiveData<List<Stat>> getAll();

}
