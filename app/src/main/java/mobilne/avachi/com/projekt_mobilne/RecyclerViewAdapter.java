package mobilne.avachi.com.projekt_mobilne;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import mobilne.avachi.com.projekt_mobilne.database.Stat;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView dateText;
        TextView steps;
        TextView calories;

        public ViewHolder(View v){
            super(v);
            dateText = v.findViewById(R.id.dateText);
            steps = v.findViewById(R.id.stepsText);
            calories = v.findViewById(R.id.caloriesText);
        }
    }

    protected Context context;
    private List<Stat> statList;
    JSONArray jsonArray;

    public RecyclerViewAdapter(Context context, ArrayList<Stat> statList){
        this.context = context;
        this.statList = new ArrayList<>(statList);
        jsonArray = new JSONArray();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_detail, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
            viewHolder.dateText.setText(statList.get(i).getData());
            viewHolder.steps.setText(String.valueOf(statList.get(i).getSteps()));
            viewHolder.calories.setText(String.valueOf(statList.get(i).getCalories()));
    }

    @Override
    public int getItemCount() {
        if(statList != null){
            return statList.size();
        }
        return 0;
    }

    public void updateList(ArrayList<Stat> list){
        statList = new ArrayList<>(list);
    }
}
