package mobilne.avachi.com.projekt_mobilne;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {
    private RadioGroup radioGroup, languageRadioGroup;
    private RadioButton maleButton, femaleButton;
    EditText name, weight, goal;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        name = findViewById(R.id.nameEdit);
        weight = findViewById(R.id.weightEdit);
        goal = findViewById(R.id.goalEdit);

        radioGroup = findViewById(R.id.radioGroup2);
        maleButton = findViewById(R.id.maleRadioButton);
        femaleButton = findViewById(R.id.femaleRadioButton);

        final SharedPreferences settings = getSharedPreferences("dane", MODE_PRIVATE);
        if(settings.getBoolean("hi5", false)) {
            String namePref = settings.getString("nick", "null");
            if (namePref.equals("null")) name.setHint(R.string.putName);
            else name.setText(namePref);

            float weightPref = settings.getFloat("weight", 0f);
            if (weightPref == 0f) weight.setHint(R.string.putWeight);
            else weight.setText(String.valueOf(weightPref));

            int goalPref = settings.getInt("goal", 0);
            if (goalPref == 0) goal.setHint(R.string.putGoal);
            else goal.setText(String.valueOf(goalPref));

            boolean sexPref = settings.getBoolean("male", true);
            if (sexPref == true) maleButton.setChecked(true);
            else femaleButton.setChecked(true);
        }else{
            name.setHint(R.string.putName);
            weight.setHint(R.string.putWeight);
            goal.setHint(R.string.putGoal);
        }


        Button button = findViewById(R.id.submitSettings);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("hi5", true);
                if(!name.getText().toString().isEmpty()) editor.putString("nick", name.getText().toString());
                else editor.putString("nick", "default");
                if(Float.parseFloat(weight.getText().toString())<30) editor.putFloat("weight", 30);
                else if(Float.parseFloat(weight.getText().toString())>250) editor.putFloat("weight", 250);
                else editor.putFloat("weight", Float.parseFloat(weight.getText().toString()));
                if(Integer.parseInt(goal.getText().toString())>99999) editor.putInt("goal", 99999);
                else editor.putInt("goal", Integer.parseInt(goal.getText().toString()));

                if(radioGroup.getCheckedRadioButtonId() == maleButton.getId()) editor.putBoolean("male", true);
                else editor.putBoolean("male", false);
                editor.commit();
                finish();
            }
        });

    }

    public void setLocale(String language){
        Resources resources = getResources();
        Locale locale = new Locale(language);
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, displayMetrics);
    }

}
