package mobilne.avachi.com.projekt_mobilne;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobilne.avachi.com.projekt_mobilne.retrofit.News;
import mobilne.avachi.com.projekt_mobilne.retrofit.NewsListAdapter;

public class NewsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        final ArrayList<News> newsList = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final NewsListAdapter newsListAdapter = new NewsListAdapter(this, newsList);
        recyclerView.setAdapter(newsListAdapter);

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,"https://api.myjson.com/bins/18fx14", null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray(("news"));

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject news = jsonArray.getJSONObject(i);

                                String topic = news.getString("topic");
                                String url = news.getString("image");
                                String description = news.getString("description");

                                newsList.add(new News(topic, description, url));
                            }
                            newsListAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        queue.add(request);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Intent intent;
        switch(item.getItemId()){
            case R.id.menu:
                intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuSettings:
                intent = new Intent(this, SettingsActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuStats:
                intent = new Intent(this, StatInfoActivity.class);
                this.startActivity(intent);
                break;
            case R.id.menuNews:
                intent = new Intent(this, NewsActivity.class);
                this.startActivity(intent);
                break;
        }
        return true;
    }
}
