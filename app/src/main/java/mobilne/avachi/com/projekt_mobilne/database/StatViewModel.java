package mobilne.avachi.com.projekt_mobilne.database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.sql.Date;
import java.time.OffsetDateTime;
import java.util.List;

public class StatViewModel extends AndroidViewModel {
    private StatRepository statRepository;
    private LiveData<List<Stat>> allStats;
    private LiveData<Stat> stat;

    public StatViewModel(@NonNull Application application) {
        super(application);
        statRepository = new StatRepository(application);
        allStats = statRepository.getAllStats();
    }

    public LiveData<Stat> getStat(String date){
        stat = statRepository.findStatByDate(date);
        return  stat;
    }

    public LiveData<List<Stat>> getAllStats(){return allStats;}

    public void insert(Stat stat){statRepository.insert(stat);}
    public void update(Stat stat){statRepository.update(stat);}
    public void delete(Stat stat){statRepository.delete(stat);}
}
