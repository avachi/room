package mobilne.avachi.com.projekt_mobilne.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.sql.Date;
import java.time.OffsetDateTime;
import java.util.List;

public class StatRepository {
    private StatDao statDao;
    private LiveData<List<Stat>> allStats;

    StatRepository(Application application){
        StatsRoomDatabase db = StatsRoomDatabase.getDatabase(application);
        statDao = db.statDao();
        allStats = statDao.getAll();
    }

    LiveData<List<Stat>> getAllStats(){ return  allStats;}

    public LiveData<Stat> findStatByDate(String date){return statDao.findByDay(date);}

    public void insert(Stat stat){new insertAsyncStat(statDao).execute(stat);}
    public void update(Stat stat){new updateAsyncStat(statDao).execute(stat);}
    public void delete(Stat stat){new deleteAsyncStat(statDao).execute(stat);}



    private static class updateAsyncStat extends AsyncTask<Stat, Void, Void>{
        private StatDao statDao;
        updateAsyncStat(StatDao statDao){this.statDao = statDao;}

        @Override
        protected Void doInBackground(final Stat... stat){
            statDao.update(stat[0]);
            statDao.update(stat[0]);
            return null;
        }
    }

    private static class insertAsyncStat extends AsyncTask<Stat, Void, Void> {

        private StatDao statDao;

        insertAsyncStat(StatDao dao) {
            statDao = dao;
        }

        @Override
        protected Void doInBackground(final Stat... params) {
            statDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteAsyncStat extends AsyncTask<Stat, Void, Void> {
        private StatDao statDao;

        deleteAsyncStat(StatDao statDao){this.statDao = statDao;}
        @Override
        protected Void doInBackground(final Stat... product){
            statDao.delete(product[0]);
            return null;
        }
    }
}
