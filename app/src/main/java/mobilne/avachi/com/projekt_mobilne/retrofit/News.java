package mobilne.avachi.com.projekt_mobilne.retrofit;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class News implements Serializable {

    @SerializedName("topic")
    private String topic;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;




    public News(String topic, String description, String url) {
        this.topic = topic;
        this.description = description;
        this.image = url;
    }


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
